Pod::Spec.new do |s|

  s.name         = "VLCMediaLibraryKit"
  s.version      = '0.13.0a11'
  s.summary      = "A MediaLibrary framework in Objective-C for iOS and OS X"

  s.description  = <<-DESC
                   A MediaLibrary framework in C++ wrapped in Objective-C for iOS
                   DESC

  s.homepage     = "https://code.videolan.org/videolan/VLCMediaLibraryKit"

  s.license      = { :type => 'LGPLv2.1', :file => 'COPYING' }

  s.authors            = { "Soomin Lee" => "bubu@mikan.io", "Felix Paul Kühne" => "fkuehne@videolan.org", "Carola Nitz" => "caro@videolan.org" }
  s.social_media_url   = "http://twitter.com/videolan"

  s.ios.deployment_target = '9.0'
  s.tvos.deployment_target = '11.0'
  s.visionos.deployment_target = '1.0'

  s.source = {
   :http => 'https://download.videolan.org/pub/cocoapods/unstable/VLCMediaLibraryKit-0.13.0a11-941eca0-3fd80d50.zip',
   :sha256 => '98b28a4034d6cc00cac5b37131d6bcf95544617680f400771495e73520150654'
  }
  s.vendored_framework = 'VLCMediaLibraryKit.xcframework'

  s.frameworks = "Foundation"

  s.requires_arc = true

  s.dependency "VLCKit", '>= 4.0.0a10'
  s.xcconfig = {
    'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
    'CLANG_CXX_LIBRARY' => 'libc++'
  }

  # Exclude x86_64 architecture for xrSimulator
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=xrsimulator*]' => 'x86_64' }
end
